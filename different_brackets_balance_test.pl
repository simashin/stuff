#!/usr/bin/perl

our $res_balance = 1;
my $string = q! (){} { }<>[] {({[<>]})}!;

my %bracket_balance = (
    '(' => ')',
    '[' => ']',
    '{' => '}',
    '<' => '>'
);
my $level = 0;
my $balance;

while ($string =~ /([(){}\[\]<>])/g) {
    my $bracket = $1;
    if ($1 =~ /[\[{<(]/) {
        push @{$balance->{$level}}, $bracket;
        ++$level;
    } else {
        --$level;
        push @{$balance->{$level}}, $bracket;
    }
    check_balance($balance);
}
die "Balance is not ok" if scalar keys %$balance ;

print "Balance is OK";

sub check_balance {
    my ($balance) = @_;
    foreach my $lvl (keys %$balance) {
        if ( scalar @{$balance->{$lvl}} > 1) {
            if ( $balance->{$lvl}[1] eq $bracket_balance{$balance->{$lvl}[0]}) {
                delete $balance->{$lvl};
            } 
            else {
                die "Balance is not ok";
            }
        }
    }
}
